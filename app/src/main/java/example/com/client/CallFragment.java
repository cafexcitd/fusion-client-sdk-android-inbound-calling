package example.com.client;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.camera2.CameraManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alicecallsbob.fcsdk.android.phone.Call;
import com.alicecallsbob.fcsdk.android.phone.CallCreationWithErrorException;
import com.alicecallsbob.fcsdk.android.phone.CallListener;
import com.alicecallsbob.fcsdk.android.phone.CallStatus;
import com.alicecallsbob.fcsdk.android.phone.CallStatusInfo;
import com.alicecallsbob.fcsdk.android.phone.Phone;
import com.alicecallsbob.fcsdk.android.phone.PhoneListener;
import com.alicecallsbob.fcsdk.android.phone.PhoneVideoCaptureSetting;
import com.alicecallsbob.fcsdk.android.phone.VideoSurface;
import com.alicecallsbob.fcsdk.android.phone.VideoSurfaceListener;

import java.util.List;

/**
 * Created by Nathan on 07/04/2016.
 */
public class CallFragment extends Fragment implements VideoSurfaceListener, CallListener, PhoneListener {

    private VideoSurface remoteSurface = null;
    private VideoSurface localSurface = null;

    // audio / video configuration fields
    private CheckBox audioCheckBox;
    private CheckBox videoCheckBox;
    private CheckBox rearCamCheckBox;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_call, container, false);

        // make the log scrollable & clear any lorem ipsum text
        TextView logView = (TextView) view.findViewById(R.id.log);
        logView.setMovementMethod(new ScrollingMovementMethod());
        logView.setText("");

        // wire up the local audio / video & camera change check boxes
        this.audioCheckBox = (CheckBox)view.findViewById(R.id.audio);
        this.videoCheckBox = (CheckBox)view.findViewById(R.id.video);
        this.rearCamCheckBox = (CheckBox)view.findViewById(R.id.rear_cam);

        // wire up the dial / hangup
        Button dialButton = (Button) view.findViewById(R.id.dial);
        final CallListener callListener = this;
        dialButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View button) {
                // hide the keyboard
                hideKeyboard();

                // find the address the user dialled
                EditText addressField = (EditText) view.findViewById(R.id.address);
                String address = addressField.getText().toString();

                // determine the media capabilities
                boolean audio = audioCheckBox.isChecked();
                boolean video = videoCheckBox.isChecked();

                // ensure that the video views are set up
                setupVideoViews(view);

                try {
                    // create the call
                    Call call = ConfigFragment.UC.getPhone().createCall(address, audio, video, callListener);

                    // determine where the remote video stream should be displayed
                    call.setVideoView(remoteSurface);
                } catch (Exception e) {
                    e.printStackTrace();
                    addMessageToLog("Error: " + e.getMessage());
                }

            }
        });

        Button hangupButton = (Button) view.findViewById(R.id.hangup);
        hangupButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View button) {
                // hide the keyboard
                hideKeyboard();

                // hangup a call (if we're on one)
                List<? extends Call> calls = ConfigFragment.UC.getPhone().getCurrentCalls();
                for(Call call : calls) {
                    call.end();
                }

                // add the message to the log
                addMessageToLog("User clicked to hangup");
            }
        });

        audioCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton button, boolean checked) {
                addMessageToLog("Enable audio: " + checked);
            }
        });

        videoCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton button, boolean checked) {
                addMessageToLog("Enable video: " + checked);
            }
        });

        rearCamCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton button, boolean checked) {
                addMessageToLog("Use rear cam: " + checked);
            }
        });

        // return the prepared statement
        return view;
    }

    // adds a message to the log
    private void addMessageToLog(final String message) {
        final TextView textView = (TextView) this.getView().findViewById(R.id.log);
        this.getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                textView.setText(message + "\n" + textView.getText());
            }
        });
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getView().getWindowToken(), 0);
    }



    //////////////////////////////////////////
    //
    // Ensures that the video views are set up

    private void setupVideoViews(View view) {

        int width = 640;
        int height = 480;
        Point remoteDimensions = new Point(width, height);
        Point localDimensions = new Point(width / 4, height / 4);

        // construct the VideoSurface's
        Phone phone = ConfigFragment.UC.getPhone();
        this.remoteSurface = phone.createVideoSurface(this.getActivity(), remoteDimensions, this);
        this.localSurface = phone.createVideoSurface(this.getActivity(), localDimensions, this);

        // get references to the local / remote video surface containers in the UI
        RelativeLayout remoteContainer = (RelativeLayout) view.findViewById(R.id.remote);
        RelativeLayout localContainer = (RelativeLayout) view.findViewById(R.id.local);

        // empty the views
        remoteContainer.removeAllViews();
        localContainer.removeAllViews();

        // add the VideoSurface's to the UI
        remoteContainer.addView(remoteSurface);
        localContainer.addView(localSurface);

        // now set the preview view on the phone
        phone.setPreviewView(localSurface);
        phone.setCamera(Camera.CameraInfo.CAMERA_FACING_FRONT);
    }




    //////////////////////////////////////////
    //
    // VideoSurfaceListener methods

    @Override
    public void onFrameSizeChanged(int width, int height, VideoSurface.Endpoint endpoint, VideoSurface videoSurface) {
        this.addMessageToLog("onFrameSizeChanged: " + width + ", " + height + " : " + endpoint);
    }

    @Override
    public void onSurfaceRenderingStarted(VideoSurface videoSurface) {
        this.addMessageToLog("onSurfaceRenderingStarted");
    }



    //////////////////////////////////////////
    //
    // Call Listener methods

    @Override
    public void onDialFailed(Call call, String message, CallStatus callStatus) {
        this.addMessageToLog("onCallFailed: " + message);
    }

    @Override
    public void onCallFailed(Call call, String message, CallStatus callStatus) {
        this.addMessageToLog("onCallFailed: " + message);
    }

    @Override
    public void onMediaChangeRequested(Call call, boolean hadAudio, boolean hadVideo) {
        this.addMessageToLog("onMediaChangeRequested: " + hadAudio + ", " + hadVideo);
    }

    @Override
    public void onStatusChanged(Call call, CallStatus callStatus) {
        this.addMessageToLog("onStatusChanged: " + callStatus);
    }

    @Override
    public void onStatusChanged(Call call, CallStatusInfo callStatusInfo) {
        this.addMessageToLog("onStatusChanged: " + callStatusInfo);
    }

    @Override
    public void onRemoteDisplayNameChanged(Call call, String name) {
        this.addMessageToLog("onRemoteDisplayNameChanged: " + name);
    }

    @Override
    public void onRemoteMediaStream(Call call) {
        this.addMessageToLog("onRemoteMediaStream");
    }

    @Override
    public void onInboundQualityChanged(Call call, int i) {
        this.addMessageToLog("onInboundQualityChanged: " + i);
    }

    @Override
    public void onRemoteHeld(Call call) {
        this.addMessageToLog("onRemoteHeld");
    }

    @Override
    public void onRemoteUnheld(Call call) {
        this.addMessageToLog("onRemoteUnheld");
    }




    //////////////////////////////////////////
    //
    // PhoneListener methods

    public void onIncomingCall(final Call call) {
        this.addMessageToLog("onIncomingCall");

        // prompt the user to accept or reject the call
        // remember that the UI stuff must be queued on the main
        // UI thread - so need to wrap code in a Runnable that
        // we schedule for execution on the main thread.
        call.addListener(this);
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder
                    .setTitle("Incoming Call")
                    .setMessage("Incoming call from: " + call.getRemoteAddress())
                    .setCancelable(false)
                    .setPositiveButton("Answer", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            addMessageToLog("User accepted call");

                            // answer the call with audio and video
                            call.answer(audioCheckBox.isChecked(), videoCheckBox.isChecked());

                            // make sure that the video surfaces are assigned
                            // note that while not all of the actions in setupVideoViews should be
                            // done following the call to answer the call, the call to set which
                            // camera to use should be done following accepting the call
                            setupVideoViews(getView());
                            call.setVideoView(remoteSurface);
                        }
                    })
                    .setNegativeButton("Reject", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            addMessageToLog("User rejected call");
                            call.end();
                        }
                    })
                    .create()
                    .show();
            }
        };

        // now queue on the main UI thread
        this.getActivity().runOnUiThread(runnable);
    }

    public void onCaptureSettingChange(PhoneVideoCaptureSetting vcs, int val) {
        this.addMessageToLog("onCaptureSettingChange");
    }

    public void onLocalMediaStream() {
        this.addMessageToLog("onLocalMediaStream");
    }
}
